

# Figure 1
to.pdf({
  palette(rainbow(3))
  with(allom, plot(diameter, height, pch=19, col=species))},
  file="manuscript/figures/Figure1.pdf", width=6, height=5)



# Figure S1
to.pdf({
  boxplot(diameter ~ species, data=allom)
},file="manuscript/figures/FigureS1.pdf", width=6, height=5)
